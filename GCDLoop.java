import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GCDLoop {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        if ( a <0)
        {
            a = -a;
        }
        else if ( b < 0)
        {
            b = -b;

        }

        if ( a > 0 && b > 0 ) 
        {
            while ( a!= 0 && b!= 0)  
            {
                if (a >b) 
                {
                    a = a % b;

                }
                else
                {
                    b = b % a;
                }
            }

        if ( a == 0) {
            System.out.println(b);
        }
        else if ( b == 0) 
        {
            System.out.println(a);
        }
        
    }


        else if (a == 0)
        {
            System.out.println(b);
        }
        else if ( b == 0)
        {
            System.out.println(a);
        }

    }

}