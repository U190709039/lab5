
public class GCDRec {
	
	public static int GCD(int a,int b) {
        if ( a >= 0 && b>=0 )
        {
            if(b==0) 
            {
                return a;
            }
            else if ( a == 0) 
            {
                return b;
            }
            else if( a == 0 && b == 0) 
            {
                return 1;
            }
            
            else
            {
                return GCD(b,a % b);
            }
        }
        else 
        {
            a = -a;
            b = -b;
            return GCD(b,a%b);

        }
        }

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		System.out.println(GCD(a,b));
		
		

	}

}
