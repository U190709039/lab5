import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

	public static void main(String[] args) throws FileNotFoundException{
		int matrix[][] = readMatrix();
		
		printMatrix(matrix);
		boolean found = false;
search: for (int i=0; i< matrix.length; i++) {
			for (int j=0; j < matrix[i].length; j++) {
				if (search(0,matrix, i,j,false)){
					if(nextSearch(0, matrix, i, j, false))
					{
						
						if(nextSearch(0, matrix, i, j, true)){
							matrix[i][j]=9;
							found = true;
							break search;
						}
					
					}
				}
			}
		}
		
					
		if (found) {
			System.out.println("A sequence is found");
		}
		printMatrix(matrix);

	} 

	private static boolean nextSearch(int number, int[][]matrix, int row, int col, Boolean find){

		if (number<9){
			number=number+1;
			if(search(number, matrix, row, col-1, find) && col>0){
				if(nextSearch(number, matrix, row, col-1, find) && col>0){
					return true;
				}
				else{
					return false;
				}			
			}
			else if(search(number, matrix, row, col+1, find) && col<8){
				if(nextSearch(number, matrix, row, col+1, find) && col<8){
					return true;
				}
				else{
					return false;
				}
			}
			else if(search(number, matrix, row-1, col, find) && row>0){
				if(nextSearch(number, matrix, row-1, col, find) && row>0){
					return true;
				}
				else{
					return false;
				}
			}
			else if(search(number, matrix, row+1, col, find) && row<8){
				if(nextSearch(number, matrix, row+1, col, find) && row<8){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}

		}
		else{
			return true;
		}

	}
	
	private static boolean search (int number, int[][]matrix, int row, int col, boolean find) {

		if(number == matrix[row][col] && (row > 0 && col >0 && row <8 && col<8))
		{
			if(find){
				matrix[row][col]=(9-number);
			}
			return true;
		}
		else
		{
			return false;
		}

	}

	private static int[][] readMatrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("matrix.txt");

		try (Scanner sc = new Scanner(file)){

			
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
